Insert into driver(id,driver_name) values (1,'Driver 1');
Insert into driver(id,driver_name) values (2,'Driver 2');
insert into team(id,team_name) values(1,'Team 1');
insert into team(id,team_name) values(2,'Team 2');
insert into event(id,event_description) values(1,'Event 1');

insert into rel_event_team(team_id, event_id) values(1,1);
insert into rel_event_team(team_id, event_id) values(2,1);
