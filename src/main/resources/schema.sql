CREATE TABLE DRIVER(
                                  "ID" BIGINT NOT NULL,
                                  "DRIVER_NAME" VARCHAR(255)
);

ALTER TABLE DRIVER ADD CONSTRAINT "PK_DRIVER" PRIMARY KEY("ID");

CREATE TABLE TEAM(
                                "ID" BIGINT NOT NULL,
                                "TEAM_NAME" VARCHAR(255)
);
ALTER TABLE TEAM ADD CONSTRAINT "PK_TEAM" PRIMARY KEY("ID");

CREATE TABLE REL_TEAM_DRIVER(
    "DRIVER_ID" BIGINT NOT NULL,
    "TEAM_ID" BIGINT NOT NULL
);

ALTER TABLE REL_TEAM_DRIVER ADD CONSTRAINT "CONSTRAINT_F" PRIMARY KEY("TEAM_ID", "DRIVER_ID");

CREATE TABLE EVENT(
                                 "ID" BIGINT NOT NULL,
                                 "EVENT_DESCRIPTION" VARCHAR(255)
);

ALTER TABLE EVENT ADD CONSTRAINT "PK_EVENT" PRIMARY KEY("ID");

CREATE TABLE REL_EVENT_TEAM(
    "TEAM_ID" BIGINT NOT NULL,
    "EVENT_ID" BIGINT NOT NULL
);
ALTER TABLE REL_EVENT_TEAM ADD CONSTRAINT "CONSTRAINT_2" PRIMARY KEY("EVENT_ID", "TEAM_ID");

CREATE TABLE REL_EVENT_DRIVER(
                                             "DRIVER_ID" BIGINT NOT NULL,
                                             "EVENT_ID" BIGINT NOT NULL
);
ALTER TABLE REL_EVENT_DRIVER ADD CONSTRAINT "CONSTRAINT_D" PRIMARY KEY("EVENT_ID", "DRIVER_ID");

ALTER TABLE REL_TEAM_DRIVER ADD CONSTRAINT "FK_REL_TEAM__DRIVER__DRIVER_ID" FOREIGN KEY("DRIVER_ID") REFERENCES "DRIVER"("ID") NOCHECK;
ALTER TABLE REL_EVENT_DRIVER ADD CONSTRAINT "FK_REL_EVENT__DRIVER__DRIVER_ID" FOREIGN KEY("DRIVER_ID") REFERENCES "DRIVER"("ID") NOCHECK;
ALTER TABLE REL_TEAM_DRIVER ADD CONSTRAINT "FK_REL_TEAM__DRIVER__TEAM_ID" FOREIGN KEY("TEAM_ID") REFERENCES "TEAM"("ID") NOCHECK;
ALTER TABLE REL_EVENT_DRIVER ADD CONSTRAINT "FK_REL_EVENT__DRIVER__EVENT_ID" FOREIGN KEY("EVENT_ID") REFERENCES "EVENT"("ID") NOCHECK;
ALTER TABLE REL_EVENT_TEAM ADD CONSTRAINT "FK_REL_EVENT__TEAM__TEAM_ID" FOREIGN KEY("TEAM_ID") REFERENCES "TEAM"("ID") NOCHECK;
ALTER TABLE REL_EVENT_TEAM ADD CONSTRAINT "FK_REL_EVENT__TEAM__EVENT_ID" FOREIGN KEY("EVENT_ID") REFERENCES "EVENT"("ID") NOCHECK;
