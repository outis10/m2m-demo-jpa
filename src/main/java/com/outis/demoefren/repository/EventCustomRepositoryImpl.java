package com.outis.demoefren.repository;

import com.outis.demoefren.domain.Event;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Repository
public class EventCustomRepositoryImpl implements EventCustomRepository{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Event> fetchRelationships(Optional<Event> event) {
        return event.map(this::fetchTeams);
    }

    private Event fetchTeams(Event event) {
        return entityManager
                .createQuery(
                        "select event from Event event left join fetch event.teams where event is :event",
                        Event.class
                )
                .setParameter("event", event)
                .getSingleResult();

    }
    /* AQUI pones El Eloquent FRANKISTEIN  de la CRAP */
}
