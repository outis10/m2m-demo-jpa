package com.outis.demoefren.repository;

import com.outis.demoefren.domain.Event;

import java.util.Optional;

public interface EventCustomRepository {

    public Optional<Event> fetchRelationships(Optional<Event> event);

}
