package com.outis.demoefren.repository;

import com.outis.demoefren.domain.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EventRepository extends EventCustomRepository, JpaRepository<Event, Long> {

        @Query("select event from Event event left join fetch event.teams where event.id is :id")
        Optional<Event> findOneWithRelationships(@Param("id") Long id);

        default Optional<Event> findOneWithRelations(Long id) {
                return this.fetchRelationships(this.findById(id));

        }





}
