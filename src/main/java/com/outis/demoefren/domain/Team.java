package com.outis.demoefren.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Team.
 */
@Entity
@Table(name = "team")
public class Team implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "team_name")
    private String teamName;

    @ManyToMany
    @JoinTable(name = "rel_team_driver", joinColumns = @JoinColumn(name = "team_id"), inverseJoinColumns = @JoinColumn(name = "driver_id"))
    @JsonIgnoreProperties(value = { "teams", "events" }, allowSetters = true)
    private Set<Driver> drivers = new HashSet<>();

    @ManyToMany(mappedBy = "teams")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "teams", "drivers" }, allowSetters = true)
    private Set<Event> events = new HashSet<>();


    public Long getId() {
        return this.id;
    }

    public Team id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeamName() {
        return this.teamName;
    }

    public Team teamName(String teamName) {
        this.setTeamName(teamName);
        return this;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Set<Driver> getDrivers() {
        return this.drivers;
    }

    public void setDrivers(Set<Driver> drivers) {
        this.drivers = drivers;
    }

    public Team drivers(Set<Driver> drivers) {
        this.setDrivers(drivers);
        return this;
    }

    public Team addDriver(Driver driver) {
        this.drivers.add(driver);
        driver.getTeams().add(this);
        return this;
    }

    public Team removeDriver(Driver driver) {
        this.drivers.remove(driver);
        driver.getTeams().remove(this);
        return this;
    }

    public Set<Event> getEvents() {
        return this.events;
    }

    public void setEvents(Set<Event> events) {
        if (this.events != null) {
            this.events.forEach(i -> i.removeTeam(this));
        }
        if (events != null) {
            events.forEach(i -> i.addTeam(this));
        }
        this.events = events;
    }

    public Team events(Set<Event> events) {
        this.setEvents(events);
        return this;
    }

    public Team addEvent(Event event) {
        this.events.add(event);
        event.getTeams().add(this);
        return this;
    }

    public Team removeEvent(Event event) {
        this.events.remove(event);
        event.getTeams().remove(this);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Team)) {
            return false;
        }
        return id != null && id.equals(((Team) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Team{" +
                "id=" + getId() +
                ", teamName='" + getTeamName() + "'" +
                "}";
    }
}
