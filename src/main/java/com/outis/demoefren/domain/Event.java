package com.outis.demoefren.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Event.
 */
@Entity
@Table(name = "event")
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "event_description")
    private String eventDescription;

    @ManyToMany
    @JoinTable(name = "rel_event_team", joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id")
    )
    @JsonIgnoreProperties(value = { "drivers", "events" }, allowSetters = true)
    private Set<Team> teams = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "rel_event_driver",
            joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "driver_id")
    )
    @JsonIgnoreProperties(value = { "teams", "events" }, allowSetters = true)
    private Set<Driver> drivers = new HashSet<>();

    public Long getId() {
        return this.id;
    }

    public Event id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventDescription() {
        return this.eventDescription;
    }

    public Event eventDescription(String eventDescription) {
        this.setEventDescription(eventDescription);
        return this;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public Set<Team> getTeams() {
        return this.teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public Event teams(Set<Team> teams) {
        this.setTeams(teams);
        return this;
    }

    public Event addTeam(Team team) {
        this.teams.add(team);
        team.getEvents().add(this);
        return this;
    }

    public Event removeTeam(Team team) {
        this.teams.remove(team);
        team.getEvents().remove(this);
        return this;
    }

    public Set<Driver> getDrivers() {
        return this.drivers;
    }

    public void setDrivers(Set<Driver> drivers) {
        this.drivers = drivers;
    }

    public Event drivers(Set<Driver> drivers) {
        this.setDrivers(drivers);
        return this;
    }

    public Event addDriver(Driver driver) {
        this.drivers.add(driver);
        driver.getEvents().add(this);
        return this;
    }

    public Event removeDriver(Driver driver) {
        this.drivers.remove(driver);
        driver.getEvents().remove(this);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Event)) {
            return false;
        }
        return id != null && id.equals(((Event) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Event{" +
                "id=" + getId() +
                ", eventDescription='" + getEventDescription() + "'" +
                "}";
    }
}
