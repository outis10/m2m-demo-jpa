package com.outis.demoefren.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Driver.
 */
@Entity
@Table(name = "driver")
public class Driver implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "driver_name")
    private String driverName;

    @ManyToMany(mappedBy = "drivers")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "drivers", "events" }, allowSetters = true)
    private Set<Team> teams = new HashSet<>();

    @ManyToMany(mappedBy = "drivers")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "teams", "drivers" }, allowSetters = true)
    private Set<Event> events = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Driver id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDriverName() {
        return this.driverName;
    }

    public Driver driverName(String driverName) {
        this.setDriverName(driverName);
        return this;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public Set<Team> getTeams() {
        return this.teams;
    }

    public void setTeams(Set<Team> teams) {
        if (this.teams != null) {
            this.teams.forEach(i -> i.removeDriver(this));
        }
        if (teams != null) {
            teams.forEach(i -> i.addDriver(this));
        }
        this.teams = teams;
    }

    public Driver teams(Set<Team> teams) {
        this.setTeams(teams);
        return this;
    }

    public Driver addTeam(Team team) {
        this.teams.add(team);
        team.getDrivers().add(this);
        return this;
    }

    public Driver removeTeam(Team team) {
        this.teams.remove(team);
        team.getDrivers().remove(this);
        return this;
    }

    public Set<Event> getEvents() {
        return this.events;
    }

    public void setEvents(Set<Event> events) {
        if (this.events != null) {
            this.events.forEach(i -> i.removeDriver(this));
        }
        if (events != null) {
            events.forEach(i -> i.addDriver(this));
        }
        this.events = events;
    }

    public Driver events(Set<Event> events) {
        this.setEvents(events);
        return this;
    }

    public Driver addEvent(Event event) {
        this.events.add(event);
        event.getDrivers().add(this);
        return this;
    }

    public Driver removeEvent(Event event) {
        this.events.remove(event);
        event.getDrivers().remove(this);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Driver)) {
            return false;
        }
        return id != null && id.equals(((Driver) o).id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Driver{" +
                "id=" + getId() +
                ", driverName='" + getDriverName() + "'" +
                "}";
    }
}
