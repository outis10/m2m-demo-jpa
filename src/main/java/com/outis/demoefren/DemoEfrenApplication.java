package com.outis.demoefren;

import com.outis.demoefren.domain.Event;
import com.outis.demoefren.domain.Team;
import com.outis.demoefren.repository.EventRepository;
import com.outis.demoefren.repository.TeamRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

@SpringBootApplication
public class DemoEfrenApplication {

    public static void main(String[] args) {
        final Logger log = LoggerFactory.getLogger(DemoEfrenApplication.class);
        ConfigurableApplicationContext context =
        SpringApplication.run(DemoEfrenApplication.class, args);
        TeamRepository teamRepository = context.getBean(TeamRepository.class);
        EventRepository eventRepository = context.getBean(EventRepository.class);
        Event event = eventRepository.findOneWithRelationships(1L).get();

        Optional<Event> event2 = eventRepository.findOneWithRelations(1L);

        log.info(event.toString());
        Set<Team> teams = event.getTeams();
        Iterator it = teams.iterator();
        while (it.hasNext()) {
            log.info(it.next().toString());
        }

        log.info("--------------------");

        Set<Team> teams2 = event2.get().getTeams();
        Iterator it2 = teams.iterator();
        while (it2.hasNext()) {
            log.info(it2.next().toString());
        }
    }

}
